package yves.kams.game.dev.gfx.core.games;

import yves.kams.game.dev.gfx.core.scenes.SceneFX;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public final class GameIntro extends SceneFX {

    public GameIntro() {
    }

    @Override
    public void onInit() {

    }

    @Override
    public void onUpdate(double dt) {

    }

    @Override
    public void onDraw(GraphicsContext gc) {

        gc.setFill(Color.web("#2D3447"));
        gc.fillRect(0, 0, this.setting.getWidth(), this.setting.getHeight());

        gc.setFont(Font.font("Gotham", 50));
        gc.setStroke(Color.WHITE);
        gc.setFill(Color.web("#2D3447"));
        gc.strokeText("GFX ENGINE",  (this.setting.getWidth() - 323) /2., (this.setting.getHeight() / 2.));
    }

    @Override
    public void onExit() {

    }
}
