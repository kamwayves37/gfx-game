package yves.kams.game.dev.gfx.core.games;

import yves.kams.game.dev.gfx.core.logger.Log;
import yves.kams.game.dev.gfx.core.scenes.SceneFX;
import yves.kams.game.dev.gfx.core.ecs.World;
import yves.kams.game.dev.gfx.core.settings.Setting;
import javafx.scene.canvas.GraphicsContext;

public final class GamePlay extends SceneFX{
    private final World world;

    public GamePlay(Setting setting) {
        this.world = new World(setting);

        setting.setWorld(this.world);
    }

    @Override
    public void onInit() {
        Log.info("GamePlay", "Init the world");

        world.onInit();
    }

    @Override
    public void onUpdate(double dt) {
        world.onUpdate(dt);
    }

    @Override
    public void onDraw(GraphicsContext context) {
        world.onDraw(context);
    }

    @Override
    public void onExit() {
        Log.info("GamePlay", "Exit the world");

        world.onExit();
    }
}
