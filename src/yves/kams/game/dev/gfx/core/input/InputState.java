package yves.kams.game.dev.gfx.core.input;

import javafx.scene.input.KeyCode;

import java.util.HashMap;
import java.util.Map;

public class InputState {
    public Map<KeyCode, Boolean> states = new HashMap<>();
    protected boolean changed = false;
    protected boolean pressed = false;
    protected boolean released = false;

    protected void reset() {
        changed = false;
        pressed = false;
        released = false;
    }

    public boolean anyChanged() {
        return this.changed;
    }

    public boolean anyPressed() {
        return this.pressed;
    }

    public boolean anyReleased() {
        return this.released;
    }
}
