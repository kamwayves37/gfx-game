package yves.kams.game.dev.gfx.core.input;

public class MouseState {
    protected double clientX = 0;
    protected double clientY = 0;
    protected boolean pressed = false;
    protected boolean released = false;

    public double getClientX() {
        return clientX;
    }

    public double getClientY() {
        return clientY;
    }

    public boolean isPressed() {
        return pressed;
    }

    public boolean isReleased() {
        return released;
    }
}
