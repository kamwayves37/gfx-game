package yves.kams.game.dev.gfx.core.input;

import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import yves.kams.game.dev.gfx.app.games.GameApp;
import yves.kams.game.dev.gfx.core.action.Action;
import yves.kams.game.dev.gfx.core.action.ActionManager;
import yves.kams.game.dev.gfx.core.listener.Listener;
import yves.kams.game.dev.gfx.core.settings.Setting;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.util.*;

public class Input implements Listener {
    private final Setting setting;

    private final Map<KeyCode, State> states;
    private final Map<MouseButton, State> mouseStates;

    private final ActionManager<KeyCode> inputManager;
    private final ActionManager<MouseButton> mouseManager;

    private final InputState inputState;
    private final MouseState mouseState;

    private final Map<KeyCode, Runnable> actionSystem;

    private final Map<KeyCode, Boolean> currentActionKey;
    private final Map<MouseButton, Boolean> currentActionButton;

    public Input(Setting setting) {
        this.setting = setting;
        this.inputManager = new ActionManager<>();
        this.mouseManager = new ActionManager<>();

        this.states = new HashMap<>();
        this.mouseStates = new HashMap<>();

        this.inputState = new InputState();
        this.mouseState = new MouseState();

        this.actionSystem = new HashMap<>();
        this.currentActionKey = new HashMap<>();
        this.currentActionButton = new HashMap<>();

        setting.setInput(this);
        setting.setInputState(inputState);
        setting.setMouseState(mouseState);
    }

    private void initDefaultInputSystem() {
        this.actionSystem.put(KeyCode.P, () -> {
            GameApp app = this.setting.getApp();
            if (app.isRunning())
                app.pause();
            else
                app.resume();
        });
    }

    @Override
    public void onInit() {
        setting
                .getScene()
                .addEventFilter(KeyEvent.ANY, this::keyEventHandler);

        setting
                .getScene()
                .addEventHandler(MouseEvent.ANY, this::mouseEventHandler);


        this.initDefaultInputSystem();
    }

    private void mouseEventHandler(MouseEvent event) {

        if (this.mouseManager.has(event.getButton())) {
            if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
                currentActionButton.put(event.getButton(), true);
            }

            if (event.getEventType() == MouseEvent.MOUSE_PRESSED ||
                    event.getEventType() == MouseEvent.MOUSE_RELEASED) {
                this.setKeyState(event.getButton(), event.getEventType() == MouseEvent.MOUSE_PRESSED);
            }
        }

        if (event.getEventType() == MouseEvent.MOUSE_MOVED ||
                event.getEventType() == MouseEvent.MOUSE_DRAGGED) {
            this.mouseState.mouseX = event.getSceneX();
            this.mouseState.mouseY = event.getSceneY();
        }

        event.consume();
    }

    private void keyEventHandler(KeyEvent event) {
        if (event.getEventType() == KeyEvent.KEY_RELEASED &&
                actionSystem.containsKey(event.getCode()))
            actionSystem.get(event.getCode()).run();

        if (this.inputManager.has(event.getCode())) {
            if (event.getEventType() == KeyEvent.KEY_PRESSED)
                this.currentActionKey.put(event.getCode(), true);

            this.setKeyState(event.getCode(),
                    event.getEventType() == KeyEvent.KEY_PRESSED);
        }

        event.consume();
    }

    private void setKeyState(MouseButton button, boolean value) {
        State state = this.getKeyState(button);
        state.prev = state.current;
        state.current = value;
    }

    private void setKeyState(KeyCode code, boolean value) {
        State state = this.getKeyState(code);
        state.prev = state.current;
        state.current = value;
    }

    private State getKeyState(MouseButton button) {
        State state = this.mouseStates.get(button);
        if (state == null) {
            state = new State();
            this.mouseStates.put(button, state);
        }

        return state;
    }

    private State getKeyState(KeyCode code) {
        State state = this.states.get(code);
        if (state == null) {
            state = new State();
            this.states.put(code, state);
        }

        return state;
    }

    public Input addUserAction(KeyCode code, Action action) {
        this.inputManager.add(code, action);
        return this;
    }

    public Input addUserAction(MouseButton code, Action action) {
        this.mouseManager.add(code, action);
        return this;
    }

    public Input addSystemAction(KeyCode code, Runnable runnable) {
        this.actionSystem.put(code, runnable);
        return this;
    }

    public boolean isUserAction(KeyCode code) {
        return this.states.get(code).current;
    }

    @Override
    public void onUpdate(double dt) {
        this.inputState.reset();

        this.currentActionKey.forEach((code, aBoolean) -> {
            if (aBoolean) {
                State state = this.getKeyState(code);

                System.out.println("Key " + code.getName());

                //Mouse pressed
                if (state.current && !state.prev) {
                    this.inputState.changed = true;
                    this.inputState.pressed = true;
                    this.inputManager
                            .get(code)
                            .begin();
                }

                //Mouse PressedDown
                if (state.current && state.prev) {
                    this.inputState.changed = true;
                    this.inputManager
                            .get(code)
                            .run();
                }

                //Mouse Released
                if (!state.current && state.prev) {
                    this.inputState.changed = true;
                    this.inputState.released = true;

                    this.inputManager
                            .get(code)
                            .finish();

                    this.currentActionKey.put(code, false);
                }

                state.prev = state.current;
            }
        });

        this.currentActionButton.forEach((button, aBoolean) -> {
            if (aBoolean) {
                State state = this.getKeyState(button);

                //Key pressed
                if (state.current && !state.prev) {
                    this.mouseState.changed = true;
                    this.mouseState.pressed = true;

                    this.mouseManager
                            .get(button)
                            .begin();

                }

                //key PressedDown
                if (state.current && state.prev) {
                    this.mouseState.changed = true;

                    this.mouseManager
                            .get(button)
                            .run();
                }

                //key Released
                if (!state.current && state.prev) {
                    this.mouseState.changed = true;
                    this.mouseState.released = true;

                    this.currentActionButton.put(button, false);

                    this.mouseManager.get(button).finish();
                }

                state.prev = state.current;
            }

        });
    }

    private void reset() {
        this.inputManager.clearAll();
    }

    @Override
    public void onExit() {
        this.reset();
        this.setting.getScene().removeEventHandler(KeyEvent.ANY, this::keyEventHandler);
    }

    private static class State {
        private boolean prev = false;
        private boolean current = false;
    }
}
