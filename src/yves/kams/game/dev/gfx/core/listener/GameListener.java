package yves.kams.game.dev.gfx.core.listener;

import java.util.LinkedList;
import java.util.List;

public final class GameListener {
    private final List<Listener> listeners;

    public GameListener() {
        this.listeners = new LinkedList<>();
    }

    public GameListener addListener(Listener listener){
        this.listeners.add(listener);
        return this;
    }

    public void onInit() {
        listeners.forEach(Listener::onInit);
    }

    public void onUpdate(double dt) {
        this.listeners.forEach(listener -> listener.onUpdate(dt));
    }

    public void onExit() {
        this.listeners.forEach(Listener::onExit);
    }
}
