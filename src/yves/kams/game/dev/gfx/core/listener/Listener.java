package yves.kams.game.dev.gfx.core.listener;

public interface Listener {
    void onInit();

    void onUpdate(double dt);

    void onExit();
}
