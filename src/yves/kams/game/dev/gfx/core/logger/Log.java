package yves.kams.game.dev.gfx.core.logger;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public final class Log {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";

    private static final SimpleDateFormat timeStamp = new SimpleDateFormat("HH:mm:ss");

    public static void info(String className, String msg) {
        System.out.println(ANSI_GREEN + (timeStamp.format(Calendar.getInstance().getTime()) + " " + className + ":                      " ).substring(0, 30) + msg + ANSI_RESET);
    }

    public static void debug(String className, String msg) {
        System.out.println(ANSI_BLUE + timeStamp.format(Calendar.getInstance().getTime()) + " " + className + ":  " + msg + ANSI_RESET);
    }

    public static void warn(String className, String msg) {
        System.out.println(ANSI_YELLOW + timeStamp.format(Calendar.getInstance().getTime()) + " " + className + ":  " + msg + ANSI_RESET);
    }

    public static void error(String className, Throwable error) {
        System.out.println(ANSI_RED + timeStamp.format(Calendar.getInstance().getTime()) + " " + className + ":  " + error.getMessage() + ANSI_RESET);
    }
}
