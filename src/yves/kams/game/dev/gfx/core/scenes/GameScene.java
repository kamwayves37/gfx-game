package yves.kams.game.dev.gfx.core.scenes;

import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

public final class GameScene {
    private final Pane parent;
    private final Group root;
    private final Group rootUI;

    private final Canvas canvas;
    private Text text;

    public GameScene(int width, int height) {
        parent = new Pane();
        root = new Group();
        rootUI = new Group();

        //parent.setStyle("-fx-background-color: #2D3447");

        initRootUI(16, height - 120);

        canvas = new Canvas(width, height);

        root.getChildren().add(canvas);

        rootUI.getChildren().add(text);

        parent.getChildren().addAll(root, rootUI);
    }

    private void initRootUI(int x, int y){
        text = new Text("");
        text.setStyle("fx-font-size: 11pt; -fx-font-smoothing-type: lcd; -fx-fill: #ff3c0e;");

        text.setTranslateX(x);
        text.setTranslateY(y);
    }

    public Pane getParent() {
        return parent;
    }

    public Group getRoot() {
        return root;
    }

    public Group getRootUI() {
        return rootUI;
    }

    public Text getText() {
        return text;
    }

    public GraphicsContext getGraphicsContext() {
        return canvas.getGraphicsContext2D();
    }
}
