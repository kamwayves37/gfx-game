package yves.kams.game.dev.gfx.core.scenes;

import yves.kams.game.dev.gfx.core.games.GameIntro;
import yves.kams.game.dev.gfx.core.games.GamePlay;
import yves.kams.game.dev.gfx.core.listener.Listener;
import yves.kams.game.dev.gfx.core.settings.Setting;
import javafx.scene.canvas.GraphicsContext;

public class SceneListener implements Listener {
    private final SceneManager manager;
    private final GraphicsContext gc;

    public SceneListener(Setting setting) {
        this.manager = new SceneManager(setting);

        this.manager
                .addScene(new GameIntro())
                .addScene(new GamePlay(setting));

        this.gc = setting.getGameScene().getGraphicsContext();
    }

    @Override
    public void onInit() {
        this.manager.onInit();
    }

    @Override
    public void onUpdate(double dt) {
        this.manager.onUpdate(dt);
        this.manager.onDraw(gc);
    }

    @Override
    public void onExit() {
        this.manager.onExit();
    }
}
