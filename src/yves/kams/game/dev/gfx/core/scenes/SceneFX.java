package yves.kams.game.dev.gfx.core.scenes;

import yves.kams.game.dev.gfx.core.settings.ReadOnlySetting;
import javafx.scene.canvas.GraphicsContext;

public abstract class SceneFX {
    protected ReadOnlySetting setting;

    public abstract void onInit();

    public abstract void onUpdate(double dt);

    public abstract void onDraw(GraphicsContext context);

    public abstract void onExit();
}
