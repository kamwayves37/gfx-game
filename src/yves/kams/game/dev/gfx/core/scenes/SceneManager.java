package yves.kams.game.dev.gfx.core.scenes;

import yves.kams.game.dev.gfx.core.logger.Log;
import yves.kams.game.dev.gfx.core.settings.ReadOnlySetting;
import javafx.beans.property.IntegerProperty;
import javafx.scene.canvas.GraphicsContext;

import java.util.ArrayList;

public final class SceneManager {
    private final ArrayList<SceneFX> scenes = new ArrayList<>();
    private final IntegerProperty activeScene;
    private final ReadOnlySetting setting;

    public SceneManager(ReadOnlySetting setting) {
        this.setting = setting;
        this.activeScene = setting.activeSceneProperty();
    }

    public SceneManager addScene(SceneFX sceneFX){
        sceneFX.setting = this.setting;
        this.scenes.add(sceneFX);
        return this;
    }

    public void onInit() {
        Log.info("SceneManager", "Init active scene: " + this.activeScene);

        scenes.get(this.activeScene.get()).onInit();
    }

    public void onUpdate(double dt) {
        scenes.get(this.activeScene.get()).onUpdate(dt);
    }

    public void onDraw(GraphicsContext context) {
        scenes.get(this.activeScene.get()).onDraw(context);
    }

    public void onExit() {
        Log.info("SceneManager", "Exit active scene: " + this.activeScene);

        scenes.get(this.activeScene.get()).onExit();
    }
}
