package yves.kams.game.dev.gfx.core.settings;

import yves.kams.game.dev.gfx.app.games.GameApp;
import yves.kams.game.dev.gfx.core.ecs.World;
import yves.kams.game.dev.gfx.core.input.Input;
import yves.kams.game.dev.gfx.core.input.InputState;
import yves.kams.game.dev.gfx.core.scenes.GameScene;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Text;

public class ReadOnlySetting {
    protected int width = 800;
    protected int height = 600;
    protected String title = "GFX";
    protected String version = "1.0";
    protected boolean debug = false;
    protected boolean resizable = false;

    protected IntegerProperty activeScene = new SimpleIntegerProperty(1);

    protected GameApp app;
    protected World world;
    protected GraphicsContext context;
    protected GameScene gameScene;
    protected InputState inputState;
    protected Input input;
    protected Scene scene;
    protected Text text;

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getTitle() {
        return title;
    }

    public String getVersion() {
        return version;
    }

    public boolean isDebug() {
        return debug;
    }

    public GameApp getApp() {
        return app;
    }

    public World getWorld() {
        return world;
    }

    public GraphicsContext getGraphicsContext() {
        return context;
    }

    public GameScene getGameScene() {
        return gameScene;
    }

    public Scene getScene() {
        return scene;
    }

    public Text getText() {
        return text;
    }

    public boolean isResizable() {
        return resizable;
    }

    public InputState getInputState() {
        return inputState;
    }

    public Input getInput() {
        return input;
    }

    public int getActiveScene() {
        return activeScene.get();
    }

    public IntegerProperty activeSceneProperty() {
        return activeScene;
    }
}
