package yves.kams.game.dev.gfx.core.settings;

import yves.kams.game.dev.gfx.app.games.GameApp;
import yves.kams.game.dev.gfx.core.ecs.World;
import yves.kams.game.dev.gfx.core.input.Input;
import yves.kams.game.dev.gfx.core.input.InputState;
import yves.kams.game.dev.gfx.core.scenes.GameScene;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Text;

public class Setting extends ReadOnlySetting{

    public Setting setWidth(int width) {
        this.width = width;
        return this;
    }

    public Setting setHeight(int height) {
        this.height = height;
        return this;
    }

    public Setting setTitle(String title) {
        this.title = title;
        return this;
    }

    public Setting setVersion(String version) {
        this.version = version;
        return this;
    }

    public Setting setDebug(boolean debug) {
        this.debug = debug;
        return this;
    }

    public void setGraphicsContext(GraphicsContext context) {
        this.context = context;
    }

    public void setGameScene(GameScene gameScene) {
        this.gameScene = gameScene;
    }

    public void setApp(GameApp app) {
        this.app = app;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public void setText(Text text) {
        this.text = text;
    }

    public void setResizable(boolean resizable){
        this.resizable = resizable;
    }

    public void setInputState(InputState inputState) {
        this.inputState = inputState;
    }

    public void setInput(Input input) {
        this.input = input;
    }
}
