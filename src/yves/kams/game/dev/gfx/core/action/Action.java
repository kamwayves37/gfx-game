package yves.kams.game.dev.gfx.core.action;

public interface Action {
    void begin();
    void run();
    void finish();
}
