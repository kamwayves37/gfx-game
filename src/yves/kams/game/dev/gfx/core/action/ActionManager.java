package yves.kams.game.dev.gfx.core.action;

import javafx.scene.input.KeyCode;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class ActionManager {
    private final Map<KeyCode, Action> actions;

    public ActionManager() {
        this.actions = new HashMap<>();
    }

    public void add(KeyCode code, Action action){
        this.actions.put(code, action);
    }

    public Action get(KeyCode code) {
        return this.actions.get(code);
    }

    public Optional<Action> getOptional(KeyCode code){
        return Optional.ofNullable(this.get(code));
    }

    public boolean has(KeyCode code){
        return this.actions.containsKey(code);
    }

    public Set<KeyCode> type(){
        return this.actions.keySet();
    }

    public void clearAll() {
        actions.clear();
    }
}
