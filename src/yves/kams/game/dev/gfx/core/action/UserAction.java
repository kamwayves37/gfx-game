package yves.kams.game.dev.gfx.core.action;

public abstract class UserAction implements Action{
    @Override
    public final void begin() {
        this.onPressed();
    }

    @Override
    public final void run() {
        this.onPressedDown();
    }

    @Override
    public final void finish() {
        this.onReleased();
    }

    protected void onPressed(){}

    protected void onPressedDown(){}

    protected void onReleased(){}
}
