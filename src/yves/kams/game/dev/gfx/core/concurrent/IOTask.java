package yves.kams.game.dev.gfx.core.concurrent;

import yves.kams.game.dev.gfx.core.logger.Log;
import javafx.concurrent.Task;

import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Function;

public abstract class IOTask<T>  {
    private static final String DEFAULT_NAME = "NoName";
    private final String name;

    private Consumer<T> successAction = t -> {};
    private Consumer<Throwable> failAction = e -> { Log.error("IOTask", e);};

    private boolean hasFailAction = false;

    public IOTask(String name) {
        this.name = name;
    }

    public IOTask() {
        this(DEFAULT_NAME);
    }

    public final String getName() {
        return name;
    }

    public final boolean isHasFailAction() {
        return hasFailAction;
    }

    public static IOTask<Void> ofVoid(Runnable runnable) {
        return ofVoid(DEFAULT_NAME, runnable);
    }

    public static IOTask<Void> ofVoid(String name, Runnable runnable) {
        return of(name, () -> {
            runnable.run();
            return null;
        });
    }

    public static <R> IOTask<R> of(String name, Callable<R> action){
        return new IOTask<R>() {
            @Override
            protected R onExecute() throws Exception {
                return action.call();
            }
        };
    }

    public static <R> IOTask<R> of(Callable<R> action){
        return of(DEFAULT_NAME, action);
    }

    public final IOTask<T> onSuccess(Consumer<T> successAction){
        this.successAction = successAction;
        return this;
    }

    public final IOTask<T> onFailure(Consumer<Throwable> failAction){
        this.failAction = failAction;
        this.hasFailAction = true;
        return this;
    }

    public final T run(){
        try {
            T value = onExecute();
            succeed(value);
            return value;
        } catch (Exception e) {
            fail(e);
            return null;
        }
    }

    public final <R> IOTask<R> then(Function<T, IOTask<R>> mapper){
        return of(this.name, () -> mapper.apply(onExecute()).onExecute());
    }

    public final <R> IOTask<R> thenWrap(Function<T, R> mapper){
        return then(t -> of(() -> mapper.apply(t)));
    }

    public final Task<T> toJavaFXTask(){
        return new Task<T>() {
            @Override
            protected T call() throws Exception {
                return onExecute();
            }

            @Override
            protected void succeeded() {
                succeed(getValue());
            }

            @Override
            protected void failed() {
                fail(getException());
            }
        };
    }

    private void succeed(T value){
        this.successAction.accept(value);
    }

    private void fail(Throwable error){
        this.failAction.accept(error);
    }

    protected abstract T onExecute() throws Exception;
}
