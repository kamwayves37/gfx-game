package yves.kams.game.dev.gfx.core.concurrent;

import yves.kams.game.dev.gfx.core.logger.Log;

import java.time.Duration;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public enum Async implements Executor {
    INSTANCE;

    private final ExecutorService service = Executors.newCachedThreadPool(new FXThreadFactory());
    private final ScheduledExecutorService scheduledService = Executors.newScheduledThreadPool(2, new FXThreadFactory());

    @Override
    public void execute(Runnable runnable) {
        this.service.execute(runnable);
    }

    public ScheduledFuture<?> schedule(Runnable action, Duration delay){
        return scheduledService.schedule(action, delay.toMillis(), TimeUnit.MILLISECONDS);
    }

    public void shutdownNow(){
        try {
            Log.info("Async", "attempt to shutdown executor");
            this.service.shutdown();
            this.scheduledService.shutdown();
            this.service.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Log.warn("Async", "tasks interrupted");
            Log.error("Async", e);
        }finally {
            if(!this.service.isTerminated() || !this.scheduledService.isTerminated()){
                Log.warn("Async", "cancel non-finished tasks");
            }

            Log.info("Async", "shutdown finished");
        }
    }

    private static class FXThreadFactory implements ThreadFactory{
        private final AtomicInteger threadNumber = new AtomicInteger(0);
        @Override
        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable, "FX Background Thread " + threadNumber.getAndIncrement());
            thread.setDaemon(false);
            thread.setPriority(Thread.NORM_PRIORITY);
            return thread;
        }
    }
}
