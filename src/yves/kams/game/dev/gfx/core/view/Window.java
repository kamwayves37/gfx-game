package yves.kams.game.dev.gfx.core.view;

import yves.kams.game.dev.gfx.core.scenes.GameScene;
import yves.kams.game.dev.gfx.core.settings.Setting;
import javafx.scene.Scene;
import javafx.stage.Stage;

public final class Window{
    private final Scene scene;
    private final GameScene gameScene;
    private final Setting setting;
    private final Stage stage;

    public Window(Stage stage, Setting setting) {
        this.stage = stage;
        this.setting = setting;

        gameScene = new GameScene(setting.getWidth(), setting.getHeight());
        scene = new Scene(gameScene.getParent());

        stage.setResizable(setting.isResizable());

        initSetting();

        initStage();
    }

    private void initStage(){
        //Default Title
        this.stage.setTitle(setting.getTitle() + " v" + setting.getVersion());

        stage.setScene(scene);
    }

    private void initSetting(){
        setting.setGraphicsContext(gameScene.getGraphicsContext());
        setting.setGameScene(gameScene);
        setting.setScene(scene);
        setting.setText(gameScene.getText());
    }

    public Scene getScene() {
        return scene;
    }

    public GameScene getGameScene() {
        return gameScene;
    }

    public void show(){
        stage.show();
    }
}
