package yves.kams.game.dev.gfx.core.events.bus;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;

public class Subscribe {
    private final EventType<Event> eventType;
    private final EventHandler<Event> eventHandler;

    @SuppressWarnings("unchecked")
    public <T extends Event> Subscribe(EventType<T> eventType, EventHandler<T> eventHandler) {
        this.eventType = (EventType<Event>) eventType;
        this.eventHandler = (EventHandler<Event>) eventHandler;
    }

    public void unSubscribe() {
        EventBus.INSTANCE.removeEventHandler(eventType, eventHandler);
    }
}
