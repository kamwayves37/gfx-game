package yves.kams.game.dev.gfx.core.events.bus;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Group;

public enum EventBus {
    INSTANCE;

    private final Group group = new Group();

    public void publish(Event event){
        group.fireEvent(event);
    }

    public <T extends Event> Subscribe addEventHandler(EventType<T> eventType, EventHandler<T> eventHandler) {
        group.addEventHandler(eventType, eventHandler);
        return new Subscribe(eventType, eventHandler);
    }

    public <T extends Event>  void removeEventHandler(EventType<T> eventType, EventHandler<T> eventHandler) {
        group.removeEventHandler(eventType, eventHandler);
    }
}