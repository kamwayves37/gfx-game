package yves.kams.game.dev.gfx.core.utils;

import yves.kams.game.dev.gfx.core.logger.Log;

import java.lang.reflect.InvocationTargetException;

public class Reflection {

    public static <T> T newInstance(Class<T> type) {
        try {
            return type.getConstructor().newInstance();
        } catch (InstantiationException |
                IllegalAccessException |
                InvocationTargetException |
                NoSuchMethodException e) {
            Log.error("Reflection", e);
        }

        throw new RuntimeException();
    }
}
