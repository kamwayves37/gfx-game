package yves.kams.game.dev.gfx.core.engine;

import yves.kams.game.dev.gfx.core.input.Input;
import yves.kams.game.dev.gfx.core.listener.GameListener;
import yves.kams.game.dev.gfx.core.logger.Log;
import yves.kams.game.dev.gfx.core.scenes.SceneListener;
import yves.kams.game.dev.gfx.core.settings.Setting;

public class GameEngine {
    private final LoopRunner runner = new LoopRunner(this::loop);
    private final GameListener listener;

    public GameEngine(Setting setting) {
        this.listener = new GameListener();

        this.listener
                .addListener(new Input(setting))
                .addListener(new SceneListener(setting));

        if (setting.isDebug()) {
            setting.getText().textProperty().bind(runner.textProperty());
        }
    }

    private void loop(double tps) {
        listener.onUpdate(tps);
    }

    public void start() {
        Log.info("GameEngine", "Start Game");

        listener.onInit();

        this.runner.start();
    }

    public void stop() {
        Log.info("GameEngine", "Stop Game");

        listener.onExit();

        this.runner.stop();
    }

    public void pause() {
        Log.info("GameEngine", "Pause Game");

        this.runner.pause();
    }

    public void resume() {
        Log.info("GameEngine", "Resume Game");

        this.runner.resume();
    }
}
