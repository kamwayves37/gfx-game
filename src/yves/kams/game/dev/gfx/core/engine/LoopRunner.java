package yves.kams.game.dev.gfx.core.engine;

import javafx.animation.AnimationTimer;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Arrays;
import java.util.function.Consumer;

public class LoopRunner {
    private double fps;
    private double tps;
    private boolean isStarted = false;
    private final FPSCounter fpsCounter = new FPSCounter();
    private Consumer<Double> consumer;

    private StringProperty text = new SimpleStringProperty("FPS: " + this.getFps() + "\n" + "TPS: " + tps);

    public String getText() {
        return text.get();
    }

    public StringProperty textProperty() {
        return text;
    }

    private AnimationTimer timer = new AnimationTimer() {
        @Override
        public void handle(long now) {
            tps = tpfCompute(now);

            frame();
        }
    };

    public LoopRunner(Consumer<Double> consumer) {
        this.consumer = consumer;
    }

    public double getFps() {
        return fps;
    }

    public double getTps() {
        return tps;
    }

    public boolean isStarted() {
        return isStarted;
    }

    public final void start(){
        if(!isStarted) {
            this.isStarted = true;
            this.timer.start();
        }
    }

    public final void stop(){
        this.timer.stop();
    }

    public final void pause(){
        this.timer.stop();
        this.fpsCounter.reset();
    }

    public final void resume(){
        this.timer.start();
    }

    private double tpfCompute(long now){
        this.fps = this.fpsCounter.update(now);

        if(this.fps  < 5 || this.fps > 55){
            this.fps = 60;
        }

        return 1.0 / this.fps;
    }

    public void frame(){
        text.set("FPS: " + this.getFps() + "\n" + "TPS: " + tps);

        this.consumer.accept(this.tps);
    }

    private static class FPSCounter{
        private static final int MAX_SAMPLE = 100;
        private final long[] frameTimes = new long[MAX_SAMPLE];
        private int index = 0;
        private boolean arrayFilled = false;
        private int frameRate = 0;


        public int update(long now){
            long oldFrameTime = frameTimes[index];
            frameTimes[index] = now;
            index = (index + 1) % frameTimes.length;

            if(index == 0){
                arrayFilled = true;
            }

            if (arrayFilled){
                long elapsedNanos = now - oldFrameTime;
                long elapsedNanosPerFrame = elapsedNanos / frameTimes.length;
                frameRate = (int) (1_000_000_000.0 / elapsedNanosPerFrame);
            }

            return frameRate;
        }

        public void reset(){
            Arrays.fill(frameTimes, 0L);
            index = 0;
            arrayFilled = false;
            frameRate = 0;
        }
    }
}
