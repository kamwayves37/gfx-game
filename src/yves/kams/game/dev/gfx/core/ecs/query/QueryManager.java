package yves.kams.game.dev.gfx.core.ecs.query;

import yves.kams.game.dev.gfx.core.ecs.components.ComponentEvent;
import yves.kams.game.dev.gfx.core.ecs.entities.EntityEvent;
import yves.kams.game.dev.gfx.core.events.bus.EventBus;
import yves.kams.game.dev.gfx.core.events.bus.Subscribe;
import javafx.event.Event;
import javafx.event.EventType;

import java.util.LinkedList;
import java.util.List;

public final class QueryManager {
    private final List<Query> queries;
    private final Subscribe subscribeComponentEvent;
    private final Subscribe subscribeEntityEvent;

    public QueryManager() {
        this.queries = new LinkedList<>();

        this.subscribeComponentEvent = EventBus.INSTANCE.addEventHandler(ComponentEvent.ANY, this::ComponentHandler);
        this.subscribeEntityEvent = EventBus.INSTANCE.addEventHandler(EntityEvent.ENTITY_REMOVED, this::entityHandler);
    }

    public void register(Query query){
        this.queries.add(query);
    }

    private void ComponentHandler(ComponentEvent event) {
        EventType<? extends Event> eventType = event.getEventType();
        if (ComponentEvent.COMPONENT_ADDED == (eventType)) {
            for (Query query : this.queries){
                if (query.results.contains(event.entity) ||
                        query.cacheAdded.contains(event.entity) ||
                        !query.key.contains(event.component.getClass()) ||
                        event.entity.componentCount() < query.key.size())
                    continue;

                if (event.entity.typesComponent().containsAll(query.key)) {
                    query.isAdded = true;
                    query.cacheAdded.add(event.entity);
                }
            }
        } else if (ComponentEvent.COMPONENT_REMOVE == (eventType)) {
            for (Query query : this.queries){
                if (query.cacheRemoved.contains(event.entity) ||
                        !query.results.contains(event.entity))
                    continue;

                if (query.key.contains(event.component.getClass())) {
                    query.isRemoved = true;
                    query.cacheRemoved.add(event.entity);
                }
            }
        }
    }

    private void entityHandler(EntityEvent event){
        for (Query query : this.queries) {
            if(query.cacheRemoved.contains(event.entity) ||
                    !query.results.contains(event.entity))
                continue;

            query.isRemoved = true;
            query.cacheRemoved.add(event.entity);
        }
    }

    public void destroy() {
        this.queries.forEach(Query::reset);

        this.subscribeComponentEvent.unSubscribe();
        this.subscribeEntityEvent.unSubscribe();
    }
}
