package yves.kams.game.dev.gfx.core.ecs.entities;

import javafx.event.Event;
import javafx.event.EventType;

public final class EntityEvent extends Event {
    public static final EventType<EntityEvent> ANY =
            new EventType<>(Event.ANY, "ENTITY EVENT");

    public static final EventType<EntityEvent> ENTITY_CREATED =
            new EventType<>(ANY, "ENTITY CREATED");

    public static final EventType<EntityEvent> ENTITY_REMOVED =
            new EventType<>(ANY, "ENTITY REMOVED");

    public final Entity entity;

    public EntityEvent(EventType<EntityEvent> eventType, Entity entity) {
        super(eventType);
        this.entity = entity;
    }

    public Entity getEntity() {
        return entity;
    }
}
