package yves.kams.game.dev.gfx.core.ecs;

import yves.kams.game.dev.gfx.core.ecs.entities.Entity;
import yves.kams.game.dev.gfx.core.ecs.entities.EntityManager;
import yves.kams.game.dev.gfx.core.ecs.systems.System;
import yves.kams.game.dev.gfx.core.ecs.systems.SystemManager;
import yves.kams.game.dev.gfx.core.events.bus.EventBus;
import yves.kams.game.dev.gfx.core.logger.Log;
import yves.kams.game.dev.gfx.core.settings.ReadOnlySetting;
import javafx.scene.canvas.GraphicsContext;

public final class World {
    private final SystemManager systemManager;
    private final EntityManager entityManager;
    private final ReadOnlySetting setting;
    private boolean enabled;

    public World(ReadOnlySetting setting) {
        this.systemManager = new SystemManager(this);
        this.entityManager = new EntityManager(this);

        this.setting = setting;

        this.enabled = true;

        EventBus.INSTANCE.publish(new WorldEvent(WorldEvent.WORLD_CREATED, this));
    }

    public ReadOnlySetting getSetting() {
        return setting;
    }

    public int entityCount() {
        return this.entityManager.count();
    }

    public void start() {
        this.enabled = true;
    }

    public void stop() {
        this.enabled = false;
    }

    public World registerSystem(System system) {
        systemManager.register(system);
        return this;
    }

    public <T extends System> World unRegisterSystem(Class<T> type) {
        if (!systemManager.unRegister(type))
            Log.warn("World", "The System " + type.getSimpleName() + " not in list");

        return this;
    }

    public <T extends System> T getSystem(Class<T> type) {
        return type.cast(systemManager.get(type));
    }

    public Entity createEntity(Enum<?> type) {
        return this.entityManager.createEntity(type);
    }

    public boolean destroyEntity(Entity entity) {
        return this.entityManager.removeEntity(entity);
    }

    public void onInit() {
        Log.info("GameWorld", "Init all Systems ...");

        systemManager.onInit();
    }

    public void onUpdate(double dt) {
        if (enabled)
            systemManager.onUpdate(dt);
    }

    public void onDraw(GraphicsContext context) {
        if (enabled)
            systemManager.onDraw(context);
    }

    public void onExit() {
        Log.info("GameWorld", "Exit all Systems ...");

        systemManager.onExit();
    }
}
