package yves.kams.game.dev.gfx.core.ecs.query;

import yves.kams.game.dev.gfx.core.ecs.components.Component;
import yves.kams.game.dev.gfx.core.ecs.entities.Entity;

import java.util.*;

public final class Query {
    protected final List<Entity> results;
    protected final List<Entity> cacheAdded;
    protected final List<Entity> cacheRemoved;
    protected final Set<Class<? extends Component>> key;

    protected boolean isAdded;
    protected boolean isRemoved;

    public Query() {
        results = new LinkedList<>();
        cacheAdded = new LinkedList<>();
        cacheRemoved = new LinkedList<>();
        this.key = new HashSet<>();
    }

    public <T extends Component> Query addKey(Class<T> type) {
        key.add(type);
        return this;
    }

    public List<Entity> getResults() {
        if (isAdded) {
            this.results.addAll(this.cacheAdded);
            this.cacheAdded.clear();
            isAdded = false;
        }

        if (isRemoved) {
            this.results.removeAll(this.cacheRemoved);
            this.cacheRemoved.clear();
            isRemoved = false;
        }

        return results;
    }

    protected void reset() {
        this.results.clear();
        this.cacheAdded.clear();
        this.cacheRemoved.clear();
    }
}
