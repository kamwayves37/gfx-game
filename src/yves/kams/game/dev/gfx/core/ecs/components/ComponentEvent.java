package yves.kams.game.dev.gfx.core.ecs.components;

import yves.kams.game.dev.gfx.core.ecs.entities.Entity;
import javafx.event.Event;
import javafx.event.EventType;

public final class ComponentEvent extends Event {
    public static final EventType<ComponentEvent> ANY =
            new EventType<>(Event.ANY, "COMPONENT EVENT");

    public static final EventType<ComponentEvent> COMPONENT_ADDED =
            new EventType<>(ANY, "COMPONENT ADDED");

    public static final EventType<ComponentEvent> COMPONENT_REMOVE =
            new EventType<>(ANY, "COMPONENT REMOVE");

    public final Entity entity;
    public final Component component;

    public ComponentEvent(EventType<ComponentEvent> eventType, Entity entity, Component component) {
        super(eventType);

        this.entity = entity;
        this.component = component;
    }
}
