package yves.kams.game.dev.gfx.core.ecs.entities;

import yves.kams.game.dev.gfx.core.ecs.World;
import yves.kams.game.dev.gfx.core.events.bus.EventBus;
import yves.kams.game.dev.gfx.core.logger.Log;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public final class EntityManager {
    private final World world;
    private final List<Entity> entities;

    public EntityManager(World world) {
        this.world = world;
        entities = new LinkedList<>();
    }

    public Entity createEntity(Enum<?> type) {
        Entity entity = new Entity(this.world);

        entity.setType(type);

        this.entities.add(entity);

        EventBus.INSTANCE.publish(new EntityEvent(EntityEvent.ENTITY_CREATED, entity));

        return entity;
    }

    public boolean removeEntity(Entity entity){
        if(!entities.contains(entity)){
            Log.warn("EntityManager", "Tried to remove entity not in list");
            return false;
        }

        entity.setActive(false);

        EventBus.INSTANCE.publish(new EntityEvent(EntityEvent.ENTITY_REMOVED, entity));

        entity.removeAllComponents();

        return this.entities.remove(entity);
    }

    public List<Entity> getEntityByType(Enum<?> type){
        return entities
                .stream()
                .filter(entity -> entity.getType() == type)
                .collect(Collectors.toList());
    }

    public int count(){
        return this.entities.size();
    }
}
