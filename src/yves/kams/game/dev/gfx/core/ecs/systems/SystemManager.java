package yves.kams.game.dev.gfx.core.ecs.systems;

import yves.kams.game.dev.gfx.core.ecs.World;
import yves.kams.game.dev.gfx.core.ecs.query.QueryManager;
import yves.kams.game.dev.gfx.core.logger.Log;
import javafx.scene.canvas.GraphicsContext;

import java.util.*;

public final class SystemManager {
    private final QueryManager queryManager;
    private final HashMap<Class<? extends System>, System> cache;
    private final List<System> systems;
    private final World world;

    public SystemManager(World world) {
        this.world = world;
        this.cache = new HashMap<>();
        this.systems = new ArrayList<>();
        this.queryManager = new QueryManager();
    }

    public <T extends System> void register(System system) {
        if(this.cache.containsKey(system.getClass())){
            Log.warn("SystemManager", "Tried to register System " + system.getClass().getSimpleName() + " exist in list");
            return;
        }

        system.setWorld(this.world);

        this.queryManager.register(system.queries);

        this.cache.put(system.getClass(), system);
        this.systems.add(system);
    }

    public <T extends System> boolean unRegister(Class<T> type) {
        System system = cache.get(type);

        if (system == null) {
            Log.warn("SystemManager", "Tried to unRegister System " + type.getSimpleName() + " not in list");
            return false;
        }

        system.setActive(false);

        return this.systems.remove(system);
    }

    public <T extends System> System get(Class<T> type) {
        return cache.get(type);
    }

    public void onInit() {
        systems.forEach(System::onInit);
    }

    public void onUpdate(double dt) {
        systems.forEach(system -> {
            if (system.isActive())
                system.onUpdate(dt);
        });
    }

    public void onDraw(GraphicsContext gc) {
        systems.forEach(system -> {
            if (system.isActive() && system.isRendered())
                system.onDraw(gc);
        });
    }

    public void onExit() {
        systems.forEach(System::onExit);
    }
}
