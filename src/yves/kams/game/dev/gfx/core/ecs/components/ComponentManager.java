package yves.kams.game.dev.gfx.core.ecs.components;

import java.util.*;

public final class ComponentManager {
    private final Map<Class<? extends Component>, Component> cache;
    private final List<Component> components;

    public ComponentManager() {
        this.cache = new HashMap<>();
        this.components = new LinkedList<>();
    }

    public void add(Component component){
        this.cache.put(component.getClass(), component);
        this.components.add(component);
    }

    public <T extends Component> Component get(Class<T> type){
        return this.cache.get(type);
    }

    public <T extends Component> boolean remove(Class<T> type){
        Component component = this.cache.remove(type);
        return this.components.remove(component);
    }

    public void removeAll(){
        this.cache.clear();
        this.components.clear();
    }

    public <T extends Component> boolean has(Class<T> type){
        return this.cache.containsKey(type);
    }

    public Set<Class<? extends Component>> type(){
        return this.cache.keySet();
    }

    public int count(){
        return this.components.size();
    }
}
