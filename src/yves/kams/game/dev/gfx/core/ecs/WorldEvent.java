package yves.kams.game.dev.gfx.core.ecs;

import javafx.event.Event;
import javafx.event.EventType;

public final class WorldEvent extends Event {

    public static final EventType<WorldEvent> ANY =
            new EventType<>(Event.ANY, "WORLD EVENT");

    public static final EventType<WorldEvent> WORLD_CREATED =
            new EventType<>(ANY, "WORLD CREATED");

    private final World world;

    public WorldEvent(EventType<WorldEvent> eventType, World world) {
        super(eventType);

        this.world = world;
    }

    public World getWorld() {
        return world;
    }
}
