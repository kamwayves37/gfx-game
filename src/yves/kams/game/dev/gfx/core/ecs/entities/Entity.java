package yves.kams.game.dev.gfx.core.ecs.entities;

import yves.kams.game.dev.gfx.core.ecs.components.ComponentEvent;
import yves.kams.game.dev.gfx.core.ecs.World;
import yves.kams.game.dev.gfx.core.ecs.components.Component;
import yves.kams.game.dev.gfx.core.ecs.components.ComponentManager;
import yves.kams.game.dev.gfx.core.events.bus.EventBus;
import yves.kams.game.dev.gfx.core.logger.Log;

import java.util.Optional;
import java.util.Set;

public final class Entity {
    private static int ID;
    private final int id;
    private Enum<?> type;
    private boolean active;
    private final World world;
    private final ComponentManager manager;

    public Entity(World world) {
        this.id = ID++;
        this.active = true;

        this.world = world;
        this.manager = new ComponentManager();
    }

    public int getId() {
        return id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public World getWorld() {
        return world;
    }

    public Enum<?> getType() {
        return type;
    }

    public void setType(Enum<?> type) {
        this.type = type;
    }

    public Entity addComponent(Component component) {
        if (hasComponent(component.getClass())) {
            Log.warn("Entity",
                    "Tried to add component " + component.getClass().getSimpleName() +
                            " that's on the entity " + this.toString());
            return this;
        }

        this.manager.add(component);

        EventBus.INSTANCE.publish(new ComponentEvent(ComponentEvent.COMPONENT_ADDED, this, component));

        return this;
    }

    public <T extends Component> T getComponent(Class<T> type) {
        Component component = this.manager.get(type);

        if (component == null)
            Log.warn("Entity",
                    "Tried to get component " + type.getSimpleName() + " not in entity " + this.toString());

        return type.cast(component);
    }

    public <T extends Component> Optional<T> getOptionalComponent(Class<T> type) {
        return Optional.ofNullable(type.cast(this.manager.get(type)));
    }

    public <T extends Component> boolean hasComponent(Class<T> type) {
        return this.manager.has(type);
    }

    public <T extends Component> Entity removeComponent(Class<T> type) {
        Component component = this.getComponent(type);

        if (component == null) {
            Log.warn("Entity",
                    "Tried to remove component " + type.getSimpleName() + " in entity " + this.toString());
            return this;
        }

        EventBus.INSTANCE.publish(new ComponentEvent(ComponentEvent.COMPONENT_REMOVE, this, component));

        if(!this.manager.remove(type))
            Log.warn("Entity",
                    "The component " + type.getSimpleName() + " could not be deleted in the entity " + this.toString());

        return this;
    }

    public void removeAllComponents() {
        this.manager.removeAll();
    }

    public Set<Class<? extends Component>> typesComponent() {
        return this.manager.type();
    }

    public int componentCount(){
        return this.manager.count();
    }

    public boolean removeToWorld() {
        return this.world.destroyEntity(this);
    }

    @Override
    public String toString() {
        return "Entity{" +
                "id=" + id +
                ", type=" + type +
                '}';
    }
}
