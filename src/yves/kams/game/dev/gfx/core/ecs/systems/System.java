package yves.kams.game.dev.gfx.core.ecs.systems;

import yves.kams.game.dev.gfx.core.ecs.World;
import yves.kams.game.dev.gfx.core.ecs.query.Query;
import yves.kams.game.dev.gfx.core.logger.Log;
import javafx.scene.canvas.GraphicsContext;

public abstract class System {
    protected boolean rendered = false;
    protected World world = null;
    protected Query queries = new Query();

    protected boolean active = true;

    public boolean isRendered() {
        return rendered;
    }

    public final World getWorld() {
        return world;
    }

    public final void setWorld(World world) {
        this.world = world;
    }

    public final boolean isActive() {
        return active;
    }

    public final void setActive(boolean active) {
        this.active = active;
    }

    public void onInit() {
        Log.info(this.getClass().getSimpleName(), "Initialised");
    }

    public void onUpdate(double dt) {/*..*/}

    public void onDraw(GraphicsContext gc) {/*..*/}

    public void onExit() {
        Log.info(this.getClass().getSimpleName(), "Finished");
    }
}
