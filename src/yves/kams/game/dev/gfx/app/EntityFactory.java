package yves.kams.game.dev.gfx.app;

import yves.kams.game.dev.gfx.app.components.*;
import yves.kams.game.dev.gfx.core.ecs.World;
import yves.kams.game.dev.gfx.core.ecs.entities.Entity;
import yves.kams.game.dev.gfx.core.settings.ReadOnlySetting;

public class EntityFactory {
    public static final double SPEED_MULTIPLIER = 80;

    public static Entity randomEntity(World world){
        if(Math.random() >= 0.5 )
            return createEnemy(world);
        else
            return createPlayer(world);
    }

    public static Entity createPlayerKeyboardSystem(World world) {
        return world
                .createEntity(EntityType.PLAYER)
                .addComponent(getRandomPosition(world.getSetting()))
                .addComponent(new VelocityComponent())
                .addComponent(new ShapeComponent("circle", 50, 50))
                .addComponent(new RenderableComponent(true));
    }

    public static Entity createPlayer(World world) {
        return world
                .createEntity(EntityType.PLAYER)
                .addComponent(getRandomPosition(world.getSetting()))
                .addComponent(getRandomVelocity())
                .addComponent(new ShapeComponent("circle", 25, 25))
                .addComponent(new IAComponent())
                .addComponent(new RenderableComponent(true));
    }

    public static Entity createEnemy(World world) {
        return world
                .createEntity(EntityType.ENEMY)
                .addComponent(getRandomPosition(world.getSetting()))
                .addComponent(getRandomVelocity())
                .addComponent(new ShapeComponent("box", 25, 25))
                .addComponent(new IAComponent())
                .addComponent(new RenderableComponent(true));
    }

    private static PositionComponent getRandomPosition(ReadOnlySetting setting) {
        return new PositionComponent(Math.random() * setting.getWidth(),
                Math.random() * setting.getHeight());
    }

    private static VelocityComponent getRandomVelocity() {
        return new VelocityComponent(SPEED_MULTIPLIER * (2 * Math.random() - 1),
                SPEED_MULTIPLIER * (2 * Math.random() - 1));
    }
}
