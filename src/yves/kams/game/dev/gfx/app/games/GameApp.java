package yves.kams.game.dev.gfx.app.games;

import yves.kams.game.dev.gfx.core.concurrent.Async;
import yves.kams.game.dev.gfx.core.concurrent.IOTask;
import yves.kams.game.dev.gfx.core.ecs.World;
import yves.kams.game.dev.gfx.core.logger.Log;
import yves.kams.game.dev.gfx.core.scenes.GameScene;
import yves.kams.game.dev.gfx.core.settings.ReadOnlySetting;
import yves.kams.game.dev.gfx.core.settings.Setting;
import yves.kams.game.dev.gfx.core.view.Window;
import yves.kams.game.dev.gfx.core.engine.GameEngine;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public abstract class GameApp extends Application {
    private GameEngine engine;
    private Window window;
    private Setting setting;
    private boolean isError;

    // TODO: 22/05/2020 Create GameState
    private boolean running;

    protected abstract void initSetting(Setting setting);

    protected ReadOnlySetting getSetting() {
        return this.setting;
    }

    protected GameScene getGameScene() {
        return window.getGameScene();
    }

    protected World getWorld() {
        return this.setting.getWorld();
    }

    public boolean isRunning() {
        return running;
    }

    public void pause(){
        this.running = false;
        this.engine.pause();
    }

    public void resume(){
        this.running = true;
        this.engine.resume();
    }

    @Override
    public void init() {

        setting = new Setting();
        setting.setApp(this);

        //Init Setting
        initSetting(this.setting);
    }

    @Override
    public void start(Stage stage) {
        Thread.setDefaultUncaughtExceptionHandler((thread, error) -> showError(error));

        stage.iconifiedProperty().addListener((observableValue, aBoolean, t1) -> {
            if (t1) {
                this.engine.pause();
            } else {
                this.engine.resume();
            }
        });

        window = new Window(stage, setting);

        engine = new GameEngine(this.setting);

        window.show();

        Task<Void> task = IOTask.ofVoid(() -> {
            this.initInputs();
            this.iniSystems();
            this.initEntities();
        })
                .onSuccess(aVoid -> {
                    this.engine.start();
                    this.running = true;
                })
                .onFailure(this::showError)
                .toJavaFXTask();

        Async.INSTANCE.execute(task);
    }

    @Override
    public void stop() {
        engine.stop();
        Async.INSTANCE.shutdownNow();
    }

    private void showError(Throwable error) {
        if (!isError) {

            this.engine.stop();

            error.printStackTrace();
            Log.error("GameApp", error);

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("An exception occurred!");
            alert.getDialogPane().setExpandableContent(new ScrollPane(new TextArea(error.toString())));
            alert.showAndWait();

            isError = true;

            Platform.exit();
            System.exit(0);
        }
    }


    protected abstract void iniSystems();

    protected abstract void initEntities();

    protected abstract void initInputs();
}
