package yves.kams.game.dev.gfx.app;

import yves.kams.game.dev.gfx.app.components.VelocityComponent;
import yves.kams.game.dev.gfx.app.games.GameApp;
import yves.kams.game.dev.gfx.app.systems.*;
import yves.kams.game.dev.gfx.core.action.UserAction;
import yves.kams.game.dev.gfx.core.ecs.entities.Entity;
import yves.kams.game.dev.gfx.core.settings.Setting;
import javafx.scene.input.KeyCode;

public class GFX extends GameApp {
    private Entity player;

    @Override
    protected void initSetting(Setting setting) {
        setting.setWidth(800)
                .setHeight(600)
                .setTitle("GameFX")
                .setVersion("1.1.0")
                .setDebug(true)
                .setResizable(false);
    }

    @Override
    protected void iniSystems() {
        this.getWorld()
                .registerSystem(new IASystem())
                .registerSystem(new MovementSystem())
                .registerSystem(new CollisionSystem())
                .registerSystem(new RendererSystem());
    }

    @Override
    protected void initEntities() {
        player = EntityFactory.createPlayerKeyboardSystem(this.getWorld());
    }

    @Override
    protected void initInputs() {
        this.getSetting()
                .getInput()
                .addUserAction(KeyCode.UP, new UserAction() {
            @Override
            protected void onPressedDown() {
                player.getOptionalComponent(VelocityComponent.class)
                        .ifPresent(velocity -> velocity.y = -80);
            }

            @Override
            protected void onReleased() {
                player.getOptionalComponent(VelocityComponent.class)
                        .ifPresent(velocity -> velocity.y = 0);
            }
        })
                .addUserAction(KeyCode.DOWN, new UserAction() {
                    @Override
                    protected void onPressedDown() {
                        player.getOptionalComponent(VelocityComponent.class)
                                .ifPresent(velocity -> velocity.y = 80);
                    }

                    @Override
                    protected void onReleased() {
                        player.getOptionalComponent(VelocityComponent.class)
                                .ifPresent(velocity -> velocity.y = 0);
                    }
                })
                .addUserAction(KeyCode.LEFT, new UserAction() {
            @Override
            protected void onPressedDown() {
                player.getOptionalComponent(VelocityComponent.class)
                        .ifPresent(velocity -> velocity.x = -80);
            }

            @Override
            protected void onReleased() {
                player.getOptionalComponent(VelocityComponent.class)
                        .ifPresent(velocity -> velocity.x = 0);
            }
        })
                .addUserAction(KeyCode.RIGHT, new UserAction() {
            @Override
            protected void onPressedDown() {
                player.getOptionalComponent(VelocityComponent.class)
                        .ifPresent(velocity -> velocity.x = 80);
            }

            @Override
            protected void onReleased() {
                player.getOptionalComponent(VelocityComponent.class)
                        .ifPresent(velocity -> velocity.x = 0);
            }
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
