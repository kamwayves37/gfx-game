package yves.kams.game.dev.gfx.app.systems;

import yves.kams.game.dev.gfx.app.components.PositionComponent;
import yves.kams.game.dev.gfx.app.components.ShapeComponent;
import yves.kams.game.dev.gfx.app.components.VelocityComponent;
import yves.kams.game.dev.gfx.core.ecs.systems.System;

public final class MovementSystem extends System {
    private int canvasWidth;
    private int canvasHeight;

    public MovementSystem() {
        this.queries
                .addKey(PositionComponent.class)
                .addKey(VelocityComponent.class);
    }

    @Override
    public void onInit() {
        super.onInit();

        this.canvasWidth = this.getWorld().getSetting().getWidth();
        this.canvasHeight = this.getWorld().getSetting().getHeight();
    }

    @Override
    public void onUpdate(double dt) {
        this.queries.getResults().forEach(entity -> {

            if(entity.isActive()){
                PositionComponent position = entity.getComponent(PositionComponent.class);
                VelocityComponent velocity = entity.getComponent(VelocityComponent.class);
                ShapeComponent shape = entity.getComponent(ShapeComponent.class);

                position.x += velocity.x * dt;
                position.y += velocity.y * dt;

                double SHAPE_HALF_SIZE = shape.w;


                if (position.x > canvasWidth + SHAPE_HALF_SIZE) position.x = -SHAPE_HALF_SIZE;
                if (position.x < -SHAPE_HALF_SIZE) position.x = canvasWidth + SHAPE_HALF_SIZE;
                if (position.y > canvasHeight + SHAPE_HALF_SIZE) position.y = -SHAPE_HALF_SIZE;
                if (position.y < -SHAPE_HALF_SIZE) position.y = canvasHeight + SHAPE_HALF_SIZE;
            }
        });
    }
}
