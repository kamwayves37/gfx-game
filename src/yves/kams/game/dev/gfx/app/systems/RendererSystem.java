package yves.kams.game.dev.gfx.app.systems;

import yves.kams.game.dev.gfx.app.components.IAComponent;
import yves.kams.game.dev.gfx.app.components.PositionComponent;
import yves.kams.game.dev.gfx.app.components.RenderableComponent;
import yves.kams.game.dev.gfx.app.components.ShapeComponent;
import yves.kams.game.dev.gfx.core.ecs.systems.System;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public final class RendererSystem extends System {

    public RendererSystem() {
        this.queries
                .addKey(RenderableComponent.class)
                .addKey(ShapeComponent.class);
    }

    @Override
    public void onInit() {
        super.onInit();

        this.rendered = true;
    }

    @Override
    public void onDraw(GraphicsContext gc) {
        gc.clearRect(0, 0,
                this.world
                        .getSetting()
                        .getWidth(),
                this.world
                        .getSetting()
                        .getHeight());

        this.queries.getResults().forEach(entity -> {
            if (entity.isActive()) {
                ShapeComponent shape = entity.getComponent(ShapeComponent.class);
                PositionComponent position = entity.getComponent(PositionComponent.class);

                if (shape.primitive.toLowerCase().contentEquals("box")) {
                    this.drawBox(gc, shape, position);
                } else {
                    this.drawCircle(gc, shape, position);
                }


                entity.getOptionalComponent(IAComponent.class).ifPresent(iaComponent -> {
                    if (iaComponent.live < 150 && iaComponent.live > 100)
                        gc.setFill(Color.web("#ee706a"));
                    else if (iaComponent.live < 100 && iaComponent.live > 50)
                        gc.setFill(Color.web("#fb3d22"));
                    else if (iaComponent.live < 50 && iaComponent.live > 20)
                        gc.setFill(Color.web("#db251e"));
                    else if (iaComponent.live < 20)
                        gc.setFill(Color.web("C00505"));


                    gc.setFont(Font.font("Gotham", 14));

                    gc.fillText("live: " + iaComponent.live, position.x - shape.w / 2, position.y - 5);
                });
            }
        });
    }

    private void drawCircle(GraphicsContext gc, ShapeComponent shape, PositionComponent position) {
        gc.setFill(Color.web("#39c495"));
        gc.fillOval(position.x, position.y, shape.w, shape.h);
    }

    private void drawBox(GraphicsContext gc, ShapeComponent shape, PositionComponent position) {
        gc.setFill(Color.web("#5005C0"));
        gc.fillRect(position.x, position.y, shape.w, shape.h);
    }
}
