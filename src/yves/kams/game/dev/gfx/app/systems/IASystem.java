package yves.kams.game.dev.gfx.app.systems;

import yves.kams.game.dev.gfx.app.EntityFactory;
import yves.kams.game.dev.gfx.app.components.*;
import yves.kams.game.dev.gfx.core.ecs.systems.System;

public final class IASystem extends System {
    public static final int MAX_ELEMENTS = 50;

    public IASystem() {
        this.queries.addKey(IAComponent.class);
    }

    @Override
    public void onInit() {
        super.onInit();

    }

    @Override
    public void onUpdate(double dt) {
        if(this.getWorld()
                .getSetting()
                .getInputState()
                .anyChanged()){
            if (Math.random() < 0.8 && this.world.entityCount() < MAX_ELEMENTS) {
                EntityFactory.randomEntity(this.world);
            }
        }

        this.queries.getResults().forEach(entity -> {
           if(entity.isActive()){
               entity.getOptionalComponent(IAComponent.class).ifPresent(iaComponent -> {
                   if(iaComponent.live > 0){
                       iaComponent.live--;
                   }else {
                       entity.removeToWorld();
                   }
               });
           }
        });
    }

    @Override
    public void onExit() {
        super.onExit();
    }
}
