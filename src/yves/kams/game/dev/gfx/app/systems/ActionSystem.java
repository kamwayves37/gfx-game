package yves.kams.game.dev.gfx.app.systems;

import yves.kams.game.dev.gfx.core.ecs.systems.System;
import yves.kams.game.dev.gfx.core.input.Input;

public final class ActionSystem extends System {
    private Input input;

    @Override
    public void onInit() {
    }

    @Override
    public void onUpdate(double dt) {
    }

    @Override
    public void onExit() {
        super.onExit();
    }
}
