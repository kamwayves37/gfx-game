package yves.kams.game.dev.gfx.app.components;

import yves.kams.game.dev.gfx.core.ecs.components.Component;

public class RenderableComponent extends Component {
    public boolean isRendered;

    public RenderableComponent(boolean isRendered) {
        this.isRendered = isRendered;
    }
}
