package yves.kams.game.dev.gfx.app.components;

import yves.kams.game.dev.gfx.core.ecs.components.Component;

public final class VelocityComponent extends Component {
    public double x;
    public double y;

    public VelocityComponent(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public VelocityComponent() {
    }
}
