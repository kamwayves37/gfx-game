package yves.kams.game.dev.gfx.app.components;

import yves.kams.game.dev.gfx.core.ecs.components.Component;

public final class PositionComponent extends Component {
    public double x;
    public double y;

    public PositionComponent(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public PositionComponent() {
        this.x = 0;
        this.y = 0;
    }
}
