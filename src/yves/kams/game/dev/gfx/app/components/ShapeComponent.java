package yves.kams.game.dev.gfx.app.components;

import yves.kams.game.dev.gfx.core.ecs.components.Component;

public class ShapeComponent extends Component {
    public final String primitive;
    public float w;
    public float h;

    public ShapeComponent(String primitive) {
        this.primitive = primitive;
        this.w = 0;
        this.h = 0;
    }

    public ShapeComponent(String primitive, float w, float h) {
        this.primitive = primitive;
        this.w = w;
        this.h = h;
    }
}
