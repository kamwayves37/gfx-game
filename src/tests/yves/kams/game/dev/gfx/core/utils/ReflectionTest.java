package yves.kams.game.dev.gfx.core.utils;

import yves.kams.game.dev.gfx.app.components.PositionComponent;
import org.junit.jupiter.api.Test;


class ReflectionTest {

    @Test
    void newInstance() {
        PositionComponent position = Reflection.newInstance(PositionComponent.class);

        position.x = 3;

        System.out.println(position.x);
    }

}