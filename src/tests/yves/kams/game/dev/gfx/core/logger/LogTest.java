package yves.kams.game.dev.gfx.core.logger;

import org.junit.jupiter.api.Test;

class LogTest {

    @Test
    void info() {
        Log.info("LogTest", "Information");
    }

    @Test
    void debug() {
        Log.debug("LogTest", "Debugging");
    }

    @Test
    void warn() {
        Log.warn("LogTest", "Warning");
    }

    @Test
    void error() {
        Log.error("LogTest", new NullPointerException("Value is null"));
    }
}