package yves.kams.game.dev.gfx.core.scenes;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SceneManagerTest {
    private SceneManager manager;

    @BeforeEach
    void setUp() {
    }

    @Test
    void onInit() {
        manager.onInit();
    }

    @Test
    void onExit() {
        manager.onExit();
    }
}